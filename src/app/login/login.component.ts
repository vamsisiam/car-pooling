import { Component, } from '@angular/core';
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  msg:any

  constructor(private router:Router, private service:LoginService) { }
  OnSubmit(data: NgForm) {
    console.log(data.value)
    if(!this.service.Login(data)){
      this.msg="Wrong Credentials"


    }
   

  }
}
